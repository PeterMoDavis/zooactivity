package com.example.zooapp

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class ZooActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_zoo)
        val zooNm : String = intent.getStringExtra("zooName").toString()
        Toast.makeText(this, "Welcome to the $zooNm", Toast.LENGTH_SHORT).show()

    }
}