package com.example.zooapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.zooapp.R
import com.example.zooapp.databinding.FragmentDetailBinding
import com.example.zooapp.databinding.FragmentFinishBinding

class DetailFragment : Fragment() {

    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    )= FragmentDetailBinding.inflate(inflater, container, false).also { fragmentDetailBinding ->
        _binding = fragmentDetailBinding
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.animalName.text = arguments?.getString("name")
        binding.animalFact.text = arguments?.getString("fact")


    }
}