package com.example.zooapp.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zooapp.R
import com.example.zooapp.ZooActivity
import com.example.zooapp.databinding.FragmentFinishBinding

class FinishFragment : Fragment() {

   private var _binding: FragmentFinishBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    )= FragmentFinishBinding.inflate(inflater, container, false).also { fragmentFinishBinding ->
        _binding = fragmentFinishBinding
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.finishNext.setOnClickListener{
             val data = "Peter's Amazing Zoo"
            val intent = Intent(this@FinishFragment.context, ZooActivity::class.java).also {intent ->
                intent.putExtra("zooName", data)
                startActivity(intent)
            }
            findNavController().navigate(R.id.action_finishFragment_to_zoo_nav_graph2)

        }

        binding.finishPrev.setOnClickListener{
            findNavController().navigate(R.id.action_finishFragment_to_gettingStartedFragment)
        }
    }
}