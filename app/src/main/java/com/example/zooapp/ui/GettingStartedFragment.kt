package com.example.zooapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zooapp.R
import com.example.zooapp.databinding.FragmentGettingStartedBinding


class GettingStartedFragment : Fragment() {

    private var _binding: FragmentGettingStartedBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentGettingStartedBinding.inflate(inflater, container, false).also {fragmentGettingStartedBinding ->
        _binding = fragmentGettingStartedBinding
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.gettingStartedNext.setOnClickListener(){
            findNavController().navigate(R.id.action_gettingStartedFragment_to_finishFragment)
        }

        binding.gettingStartedPrev.setOnClickListener(){
            findNavController().navigate(R.id.action_gettingStartedFragment_to_helloFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}