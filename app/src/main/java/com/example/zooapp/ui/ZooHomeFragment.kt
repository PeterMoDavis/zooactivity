package com.example.zooapp.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.zooapp.R
import com.example.zooapp.databinding.FragmentZooHomeBinding

class ZooHomeFragment : Fragment() {
    private var _binding: FragmentZooHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentZooHomeBinding.inflate(inflater, container, false).also { fragmentZooHomeBinding ->
         _binding = fragmentZooHomeBinding
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.eagles.setOnClickListener(){
            val bundle = bundleOf("name" to getString(R.string.eagle_name), "fact" to getString(R.string.eagle_fact))
            findNavController().navigate(R.id.action_zooHomeFragment_to_detailFragment, bundle)
        }

        binding.lions.setOnClickListener(){
            val bundle = bundleOf("name" to getString(R.string.lion_name), "fact" to getString(R.string.lion_fact))
            findNavController().navigate(R.id.action_zooHomeFragment_to_detailFragment, bundle)
        }

        binding.zebras.setOnClickListener(){
            val bundle = bundleOf("name" to getString(R.string.zebra_name), "fact" to getString(R.string.zebra_fact))
            findNavController().navigate(R.id.action_zooHomeFragment_to_detailFragment, bundle)
        }

        binding.monkeys.setOnClickListener(){
            val bundle = bundleOf("name" to getString(R.string.monkey_name), "fact" to getString(R.string.monkey_fact))
            findNavController().navigate(R.id.action_zooHomeFragment_to_detailFragment, bundle)
        }
    }
}